const router = require('express').Router();
const orderController = require('../controllers/orderController.js');
const {authenticate, merchantAuthenticate, customerAuthenticate} = require('../middlewares/authenticate.js')

router.post('/:id', customerAuthenticate, orderController.create)
router.delete('/:id', customerAuthenticate, orderController.delete)
router.put('/:id', customerAuthenticate, orderController.update)
router.get('/', customerAuthenticate, orderController.showCart)
router.get('/history', customerAuthenticate, orderController.showCartHistory)
router.put('/', customerAuthenticate, orderController.orderDone)

module.exports = router;