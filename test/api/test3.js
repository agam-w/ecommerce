process.env.NODE_ENV = 'test'

const User = require('../../models/user')
const Product = require('../../models/product')
const UserOrder = require('../../models/userOrder')

const server = require('../../app')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect()
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken');


describe ('ORDER CONTROLLER',()=>{

    var testMerchant =  {name   : 'jaffar',
                        email   : 'jaffar@gmail.com',
                        password: '12345',
                        roles   : 'merchant'}

    var testCustomer =  {name   : 'jaffar2',
                        email   : 'jaffar2@gmail.com',
                        password: '12345',
                        roles   : 'customer'}

    var testProduct  =  {name   : 'botol plastik',
                        price   : 10000,
                        stock   : 20,
                        category: 'peralatan makan',
                        detail  : 'plastik steril'
                        }

    before((done)=>{
        chai.request(server)
            .post('/api/user/')
            .send(testMerchant)
            .end((err,res)=>{
                userMerchant=res.body.result
                done()
            })
    })

    before((done)=>{
        chai.request(server)
            .post('/api/user/')
            .send(testCustomer)
            .end((err,res)=>{
                userCustomer=res.body.result
                done()
            })
    })

    before((done)=>{
        let tokenMail = userMerchant.token
        chai.request(server)
            .get(`/api/user/verify/${tokenMail}`)
            .end((err,res)=>{
                chai.request(server)
                    .post('/api/user/login')
                    .send(testMerchant)
                    .end(function (err, res) {
                        chai.request(server)
                            .post('/api/product/')
                            .set('Authorization', `Bearer ${res.body.result}`)
                            .send(testProduct)
                            .end((err,res)=>{
                                product1 = res.body.result
                                done()
                            })
                    })
                    
            })
    })

    before((done)=>{
        let tokenMail = userCustomer.token
        chai.request(server)
            .get(`/api/user/verify/${tokenMail}`)
            .end((err,res)=>{
                chai.request(server)
                    .post('/api/user/login')
                    .send(testCustomer)
                    .end(function (err, res) {
                        token = res.body.result
                        done()
                    })
                    
            })
    })

    after((done)=>{
        Product.deleteMany({}, (err, data)=>{
            done()
        })
    })

    after((done)=>{
        User.deleteMany({}, (err,data)=>{
            done()
        })
    })

    after((done)=>{
        UserOrder.deleteMany({}, (err,data)=>{
            done()
        })
    })

    it("POST /api/order/:id should not add product to cart because authentication no header", done=>{
        chai.request(server)
            .post(`/api/order/${product1._id}`)
            .send({quantity: 1})
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("can't put from header")
                done()
            })
    })

    it("POST /api/order/:id should not add product to cart because authentication token false", done=>{
        chai.request(server)
            .post(`/api/order/${product1._id}`)
            .set('Authorization', `Bearer ${token}ghfhgt`)
            .send({quantity: 1})
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("Invalid Token")
                done()
            })
    })

    it("POST /api/order/:id should not add product to cart because authentication user delete", done=>{
        jwt.sign({ _id: '5d831b5ac20e2231322511c2',roles: 'merchant' }, process.env.SECRET_KEY, (err, Xtoken)=>{
            chai.request(server)
            .post(`/api/order/${product1._id}`)
            .set('Authorization', `Bearer ${Xtoken}`)
            .send({quantity: 1})
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("there is no customer found")
                done()
            })
        })
    })

    it("POST /api/order/:id should not add product to cart", done=>{
        chai.request(server)
            .post(`/api/order/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: "nama"})
            .end((err,res)=>{
                res.should.have.status(400)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("product and user id false, or wrong value of request")
                done()
            })
    })

    it("POST /api/order/:id should add product to cart", done=>{
        chai.request(server)
            .post(`/api/order/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: 1})
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("order was added to cart")
                res.body.should.have.property('result')
                cart = res.body.result
                done()
            })
    })

    it("PUT /api/order/:id should not update product in cart", done=>{
        let order1 = cart.orders[0]
        chai.request(server)
            .put(`/api/order/${order1._id}hjashagj`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: "nama"})
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("product and user id false, or wrong value of request")
                done()
            })
    })

    it("PUT /api/order/:id should update product in cart", done=>{
        let order1 = cart.orders[0]
        chai.request(server)
            .put(`/api/order/${order1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: 2})
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("update order done")
                res.body.should.have.property('result')
                done()
            })
    })

    it("DELETE /api/order/:id should not update product in cart", done=>{
        let order1 = cart.orders[0]
        chai.request(server)
            .delete(`/api/order/${order1._id}hjashagj`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("product or user id false")
                done()
            })
    })

    it("DELETE /api/order/:id should update product in cart", done=>{
        let order1 = cart.orders[0]
        chai.request(server)
            .delete(`/api/order/${order1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("order deleted")
                res.body.should.have.property('result')
                done()
            })
    })

    it("GET /api/order/ should show active cart", done=>{
        chai.request(server)
            .get(`/api/order/`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("active cart showed")
                res.body.should.have.property('result')
                done()
            })
    })

    it("GET /api/order/history should show cart history", done=>{
        chai.request(server)
            .get(`/api/order/history`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("cart history showed")
                res.body.should.have.property('result')
                done()
            })
    })

    it("PUT /api/order/ should not checkout active cart because empty", done=>{
        chai.request(server)
            .put(`/api/order/`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("Chart is empty")
                done()
            })
    })

    it("PUT /api/order/ should not checkout active cart because product out of stock", done=>{
        chai.request(server)
            .post(`/api/order/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: 21})
            .end((err,res)=>{
                cart2 = res.body.result
                chai.request(server)
                    .put(`/api/order/`)
                    .set('Authorization', `Bearer ${token}`)
                    .end((err,res)=>{
                        res.should.have.status(422)
                        res.body.should.have.property('success').equal(false)
                        res.body.should.have.property('error')
                        done()
                    })
            })
    })

    it("PUT /api/order/ should checkout active cart", done=>{
        let order2 = cart2.orders[0]
        chai.request(server)
            .put(`/api/order/${order2._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({quantity: 2})
            .end((err,res)=>{
                chai.request(server)
                    .put(`/api/order/`)
                    .set('Authorization', `Bearer ${token}`)
                    .end((err,res)=>{
                        res.should.have.status(201)
                        res.body.should.have.property('success').equal(true)
                        res.body.should.have.property('message').equal("Order done")
                        res.body.should.have.property('result')
                        done()
                    })
            })
    })
    
})