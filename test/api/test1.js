process.env.NODE_ENV = 'test'

const User = require('../../models/user')
const UserOrder = require('../../models/userOrder')
const funcHelper = require('../../middlewares/funcHelper')

const server = require('../../app')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect()
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken');

chai.use(chaiHttp)

describe ('USER CONTROLLER',()=>{

    after((done)=>{
        User.deleteMany({}, (err)=>{
            done()
        })
    })

    after((done)=>{
        UserOrder.deleteMany({}, (err)=>{
            done()
        })
    })

    var testCustomer =  {name   : 'addrian',
                        email   : 'addrian@gmail.com',
                        password: '12345',
                        roles: 'customer'}

    var testMerchant =  {name   : 'jaffar',
                        email   : 'jaffar@gmail.com',
                        password: '12345',
                        roles: 'merchant'}

    var XtestMerchant =  {email   : 'agarez@gmail.com',
                         password: '12345',
                         roles: 'merchant'}
    
    it("GET / should show index", done=>{
    chai.request(server)
        .get(`/`)
        .end((err,res)=>{
            res.should.have.status(200)
            res.body.should.have.property('success').equal(true)
            done()
        })
    })

    it("POST /api/user should not create user", done=>{
        chai.request(server)
            .post('/api/user/')
            .send(XtestMerchant)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    it("POST /api/user should create user merchant", done=>{
        chai.request(server)
            .post('/api/user/')
            .send(testMerchant)
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                done()
            })
    })

    it("POST /api/user/ should create user customer", done=>{
        chai.request(server)
            .post('/api/user/')
            .send(testCustomer)
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                userCustomer=res.body.result
                done()
            })
    })

    it("GET /api/user/verify/:token should verify user", done=>{
        let token = userCustomer.token
        chai.request(server)
            .get(`/api/user/verify/${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                res.body.should.have.property('message').equal("email verified success")
                done()
            })
    })

    it("GET /api/user/verify/:token should not verify user because token", done=>{
        let Xtoken = funcHelper.token(20)
        chai.request(server)
            .get(`/api/user/verify/${Xtoken}`)
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    // it("GET /api/user/verify/:token should not verify user because verify date", done=>{
    //     let token = userCustomer.token
    //     let XexpVerify = new Date();
    //     XexpVerify.setDate(XexpVerify.getDate()-5)
    //     User.findByIdAndUpdate(userCustomer._id, {expToken: XexpVerify}).exec()
    //     chai.request(server)
    //         .get(`/api/user/verify/${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(422)
    //             res.body.should.have.property('success').equal(false)
    //             res.body.should.have.property('error')
    //             done()
    //         })
    // })

    it("POST /api/user/login should login user", done=>{
        chai.request(server)
            .post('/api/user/login')
            .send(testCustomer)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                token = res.body.result
                done()
            })
    })

    it("POST /api/user/login should not login because email", done=>{
        chai.request(server)
            .post('/api/user/login')
            .send(XtestMerchant)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("unverified email")
                done()
            })
    })

    it("POST /api/user/login should not login because password", done=>{
        chai.request(server)
            .post('/api/user/login')
            .send(userCustomer)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("incorrect password")
                done()
            })
    })

    it("GET /api/user/ should show current user", done=>{
        chai.request(server)
            .get('/api/user/')
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("Show current user")
                res.body.should.have.property('result')
                done()
            })
    })

    it("GET /api/user/ should not show current user because authentication no header", done=>{
        chai.request(server)
            .get('/api/user/')
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("can't put from header")
                done()
            })
    })

    it("GET /api/user/ should not show current user because authentication token false", done=>{
        chai.request(server)
            .get('/api/user/')
            .set('Authorization', `Bearer ${token}ghfhgt`)
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("Invalid Token")
                done()
            })
    })

    it("GET /api/user/ should not show current user because authentication user delete", done=>{
        jwt.sign({ _id: '5d7b0c3157213711859d99f5',roles: 'merchant' }, process.env.SECRET_KEY, (err, Xtoken)=>{
            chai.request(server)
            .get('/api/user/')
            .set('Authorization', `Bearer ${Xtoken}`)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("there is no user found")
                done()
            })  
        })
        
    })

    it("DELETE /api/user/ should delete current user", done=>{
        chai.request(server)
            .delete('/api/user/')
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("user deleted")
                res.body.should.have.property('result')
                done()
            })
    })

    it("POST /api/user/forget forget password false", done=>{
        chai.request(server)
            .post('/api/user/forget/')
            .send(XtestMerchant)
            .end((err,res)=>{
                res.should.have.status(404)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    it("POST /api/user/forget forget password true", done=>{
        chai.request(server)
            .post('/api/user/forget')
            .send(testMerchant)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("check your email to change your password!")
                res.body.should.have.property('result')
                userMerchant = res.body.result
                done()
            })
    })

    it("PUT /api/user/forget/:token should not update user password because token", done=>{
        let Xtoken = funcHelper.token(20)
        chai.request(server)
            .put(`/api/user/forget/${Xtoken}`)
            .send({password:'12345'})
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    it("PUT /api/user/forget/:token should update user password", done=>{
        User.findById(userMerchant._id).exec()
        .then((data)=>{
            tokenChange = data.token
            chai.request(server)
            .put(`/api/user/forget/${tokenChange}`)
            .send({password:'12345'})
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                res.body.should.have.property('message').equal("password change success")
                done()
            })
        })
    })

    it("PUT /api/user/forget/:token should not update user password because verify date", done=>{
        let XexpVerify = new Date();
        XexpVerify.setDate(XexpVerify.getDate()-5)
        User.findByIdAndUpdate(userMerchant._id, {expToken: XexpVerify}).exec()
        .then((data)=>{
            tokenChange = data.token
            chai.request(server)
            .put(`/api/user/forget/${tokenChange}`)
            .send({password:'12345'})
            .end((err,res)=>{
                res.should.have.status(422)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
        }) 
    })

})