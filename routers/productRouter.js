const router = require('express').Router();
const productController = require('../controllers/productController.js');
const {authenticate, merchantAuthenticate, customerAuthenticate} = require('../middlewares/authenticate.js')

router.post('/', merchantAuthenticate, productController.create)
router.get('/', authenticate, productController.showAll)
router.get('/:id', authenticate, productController.selectProduct)
router.get('/merchant/show', merchantAuthenticate, productController.showMerchant)
router.post('/:id', merchantAuthenticate, productController.uploadImage)
router.put('/:id', merchantAuthenticate, productController.update)
router.delete('/:id', merchantAuthenticate, productController.delete)

module.exports = router;