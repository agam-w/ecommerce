process.env.NODE_ENV = 'test'

const User = require('../../models/user')
const Product = require('../../models/product')

const server = require('../../app')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect()
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken');
const fs = require('fs')

chai.use(chaiHttp)

describe ('PRODUCT CONTROLLER',()=>{

    var file        = '/home/pick/BInar/week7/Ecommerce/ecommerce/public/images/botol_plastik.jpg'
    var wrongFile   = '/home/pick/BInar/week7/Ecommerce/ecommerce/app.js'

    var testMerchant =  {name   : 'jaffar',
                        email   : 'jaffar@gmail.com',
                        password: '12345',
                        roles   : 'merchant'}

    var testMerchant2 =  {name   : 'jaffar2',
                        email   : 'jaffar2@gmail.com',
                        password: '12345',
                        roles   : 'merchant'}

    var testProduct  =  {name   : 'botol plastik',
                        price   : 10000,
                        stock   : 20,
                        category: 'peralatan makan',
                        detail  : 'plastik steril'
                        }     
                        
    var testProduct2 =  {name   : 'botol kaca',
                        price   : 20000,
                        stock   : 20,
                        category: 'peralatan makan',
                        detail  : 'kaca steril'
                        } 

    before((done)=>{
        chai.request(server)
            .post('/api/user/')
            .send(testMerchant)
            .end((err,res)=>{
                userMerchant=res.body.result
                done()
            })
    })

    before((done)=>{
        chai.request(server)
            .post('/api/user/')
            .send(testMerchant2)
            .end((err,res)=>{
                userMerchant2=res.body.result
                done()
            })
    })

    before((done)=>{
        let tokenMail = userMerchant.token
        chai.request(server)
            .get(`/api/user/verify/${tokenMail}`)
            .end((err,res)=>{
                chai.request(server)
                    .post('/api/user/login')
                    .send(testMerchant)
                    .end(function (err, res) {
                        token = res.body.result
                        done()
                    })
                    
            })
    })

    before((done)=>{
        let tokenMail2 = userMerchant2.token
        chai.request(server)
            .get(`/api/user/verify/${tokenMail2}`)
            .end((err,res)=>{
                chai.request(server)
                    .post('/api/user/login')
                    .send(testMerchant2)
                    .end(function (err, res) {
                        token2 = res.body.result
                        done()
                    })
                    
            })
    })

    after((done)=>{
        Product.deleteMany({}, (err, data)=>{
            done()
        })
    })

    after((done)=>{
        User.deleteMany({}, (err,data)=>{
            done()
        })
    })

    it("POST /api/product should create product", done=>{
        chai.request(server)
            .post('/api/product/')
            .set('Authorization', `Bearer ${token}`)
            .send(testProduct)
            .end((err,res)=>{
                res.should.have.status(201)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('result')
                product1 = res.body.result
                done()
            })
    })

    it("POST /api/product should not create product because invalid req.body", done=>{
        chai.request(server)
            .post('/api/product/')
            .set('Authorization', `Bearer ${token}`)
            .send({name: 'botol kaca'})
            .end((err,res)=>{
                res.should.have.status(400)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    it("POST /api/product should not create product because authentication no header", done=>{
        chai.request(server)
            .post('/api/product/')
            .send(testProduct)
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("can't put from header")
                done()
            })
    })

    it("POST /api/product should not create product because authentication token false", done=>{
        chai.request(server)
            .post('/api/product/')
            .set('Authorization', `Bearer ${token}ghfhgt`)
            .send(testProduct)
            .end((err,res)=>{
                res.should.have.status(401)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("Invalid Token")
                done()
            })
    })

    it("POST /api/product should not create product because authentication user delete", done=>{
        jwt.sign({ _id: '5d831b5ac20e2231322511c2',roles: 'merchant' }, process.env.SECRET_KEY, (err, Xtoken)=>{
            chai.request(server)
            .post('/api/product/')
            .set('Authorization', `Bearer ${Xtoken}`)
            .send(testProduct)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("there is no merchant found")
                done()
            })
        })
    })

    it("GET /api/product should show all product", done=>{
        chai.request(server)
            .get('/api/product/')
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("all product showed")
                res.body.should.have.property('result')
                done()
            })
    })

    it("GET /api/product/:id should show selected product", done=>{
        chai.request(server)
            .get(`/api/product/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("selected product showed")
                res.body.should.have.property('result')
                done()
            })
    })

    it("GET /api/product/:id should not show selected product", done=>{
        chai.request(server)
            .get(`/api/product/${product1._id}dhajbhaj`)
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(403)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error').equal("product id false")
                done()
            })
    })

    it("PUT /api/product/:id should update product", done=>{
        chai.request(server)
            .put(`/api/product/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send(testProduct)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("Product has been update")
                res.body.should.have.property('result')
                done()
            })
    })

    it("PUT /api/product/:id should not update product", done=>{
        chai.request(server)
            .put(`/api/product/${product1._id}`)
            .set('Authorization', `Bearer ${token}`)
            .send({price: "makan"})
            .end((err,res)=>{
                res.should.have.status(400)
                res.body.should.have.property('success').equal(false)
                res.body.should.have.property('error')
                done()
            })
    })

    it("GET /api/product/merchant/show should show merchant product", done=>{
        chai.request(server)
            .get('/api/product/merchant/show')
            .set('Authorization', `Bearer ${token}`)
            .end((err,res)=>{
                res.should.have.status(200)
                res.body.should.have.property('success').equal(true)
                res.body.should.have.property('message').equal("all product merchant showed")
                res.body.should.have.property('result')
                done()
            })
    })


    // it("POST /api/product/:id should not upload product image because no image", done=>{
    //     chai.request(server)
    //         .post(`/api/product/${product1._id}`)
    //         .set('Authorization', `Bearer ${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(422)
    //             res.body.should.have.property('success').equal(false)
    //             res.body.should.have.property('error').equal("please upload file")
    //             done()
    //         })
    // })

    // it("POST /api/product/:id should not upload product image because wrong file", done=>{
    //     chai.request(server)
    //         .post(`/api/product/${product1._id}`)
    //         .attach('img', fs.readFileSync(`${wrongFile}`), 'app.js')
    //         .set('Authorization', `Bearer ${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(400)
    //             res.body.should.have.property('success').equal(false)
    //             res.body.should.have.property('error').equal("can't upload file")
    //             done()
    //         })
    // })

    // it("POST /api/product/:id should upload product image", done=>{
    //     chai.request(server)
    //         .post(`/api/product/${product1._id}`)
    //         .attach('img', fs.readFileSync(`${file}`), 'botol_plastik.jpg')
    //         .set('Authorization', `Bearer ${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(200)
    //             res.body.should.have.property('success').equal(true)
    //             res.body.should.have.property('message').equal("url image has been update")
    //             res.body.should.have.property('result')
    //             done()
    //         })
    // })

    // it("DELETE /api/product/:id should not delete product because not user own product", done=>{ 
    //     chai.request(server)
    //         .delete(`/api/product/${product1._id}`)
    //         .set('Authorization', `Bearer ${token2}`)
    //         .end((err,res)=>{
    //             res.should.have.status(404)
    //             res.body.should.have.property('success').equal(false)
    //             res.body.should.have.property('error').equal("this is not your post!")
    //             done()
    //         })
    // })

    // it("DELETE /api/product/:id should delete product", done=>{ 
    //     chai.request(server)
    //         .delete(`/api/product/${product1._id}`)
    //         .set('Authorization', `Bearer ${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(200)
    //             res.body.should.have.property('success').equal(true)
    //             res.body.should.have.property('message').equal("product deleted")
    //             res.body.should.have.property('result')
    //             done()
    //         })
    // })

    // it("DELETE /api/product/:id should not delete product because product id false", done=>{ 
    //     chai.request(server)
    //         .delete('/api/product/nknaiuhsadiub43nkni2')
    //         .set('Authorization', `Bearer ${token}`)
    //         .end((err,res)=>{
    //             res.should.have.status(404)
    //             res.body.should.have.property('success').equal(false)
    //             res.body.should.have.property('error').equal("there is nothing to delete")
    //             done()
    //         })
    // })
    
})