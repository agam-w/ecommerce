const User = require('../models/user')
const Product = require('../models/product')
const userOrder = require('../models/userOrder')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')
saltRounds = 10
const funcHelper = require('../middlewares/funcHelper')
const {success, error} = require('../middlewares/response')


const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
    create(req, res){
        var hash = bcrypt.hashSync(req.body.password, saltRounds)
        var token = funcHelper.token(20);
        req.body['token']       = token;
        req.body['expToken']    = new Date(new Date().setHours(new Date().getHours() + 6))
        req.body['password']    = hash
        User.create(req.body, (err, data)=>{
            if(err) return res.status(422).json(error(err.message))
            
            var to              = req.body.email
            var from            = 'My@Ecommerce.com'
            var subject         = 'Verify your mail in my E-Commerce';

            var link            = "http://"+req.get('host')+"/api/user/verify/"+token;
            var html            = 'Plese click link bellow, if you register at Ecommerce.com<br>';
                html            += '<br><strong><a href='+link+'>'+"Verify Email"+'</a></strong>';
                html            += '<br><br>Thanks';
                
            funcHelper.mail(to, from, subject, html)
            
            if(data.roles=='merchant') return res.status(201).json(success(data, "user merchant created"))
            let user = [{user: `${data._id}`}]
            userOrder.create(user, (err, result)=>{
                res.status(201).json(success(data, "user customer created"))
            })
        })
    },

    login(req, res){
        User.findOne({email: req.body.email,
                     isVerified: true}, (err, user)=>{
            if (user) {
                bcrypt.compare(req.body.password, user.password, function (err, result) {
                    if (result == true) {
                            jwt.sign({ _id: user._id,roles: user.roles }, process.env.SECRET_KEY, {expiresIn: '1h'},function(err, token) {
                            res.status(200).json(success(token, "token created"));
                        });
                    }
                    else res.status(403).json(error("incorrect password"));
                
            })
            } else {
                res.status(403).json(error("unverified email"));
            }
        })
    },

    show(req, res){
        User.findById(req.user,(err, user)=>{
            res.status(200).json(success(user, "Show current user"))
        })
    },

    delete(req, res){
        Product.remove({user: req.user._id}, (err, product)=>{
            User.findByIdAndDelete(req.user._id, (err, user)=>{
                res.status(200).json(success(user ,"user deleted"))
            })
                    
        })   
    },
    
    verifyEmail(req, res){
        let token = req.params.token;
        User.findOne({ token: token }, 'expToken').exec()
        .then((users)=>{
            if(Date.now()<users.expToken){
                User.findOneAndUpdate({token: req.params.token}, {isVerified: true}, (err, data)=>{
                    //if (!data) return res.status(400).json(error("the token is not exist"))
                    res.status(200).json(success(data, "email verified success"))
                })
            }
            else{
                res.status(422).json(error('Time token validations is expired, please resend email confirm'))
            }
        })
        .catch((err)=>{
            res.status(422).json(error(err))
        })
    },

    forgetPassword(req, res){
        var token = funcHelper.token(20);
        req.body['token']   = token;
        req.body['expToken'] = new Date(new Date().setHours(new Date().getHours() + 6))
        User.findOneAndUpdate({email: req.body.email}, req.body, (err, data)=>{
            if (!data) return res.status(404).json(error(err))
            var to              = data.email
            var from            = 'My@Ecommerce.com'
            var subject         = 'Change your password for E-Commerce';

            var link            = "http://"+req.get('host')+"/api/user/forget/"+token;
            var html            = 'Plese click link bellow, if you register at Ecommerce.com<br>';
                html            += '<br><strong><a href='+link+'>'+"Reset Password"+'</a></strong>';
                html            += '<br><br>Thanks';
                
            funcHelper.mail(to, from, subject, html)
            res.status(200).json(success(data, "check your email to change your password!"))
        })
    },

    changePassword(req, res){
        let token = req.params.token;

        User.findOne({ token: token }, 'expToken').exec()
            .then((users)=>{
                if(Date.now()<users.expToken){
                    bcrypt.hash(req.body.password, saltRounds, (err, hash)=>{
                        User.findOneAndUpdate({token: req.params.token}, {password: hash}, (err, data)=>{
                            res.status(200).json(success(data, "password change success"))
                      })  
                    })
                }
                else{
                    res.status(422).json(error('Time token validations is expired, please resend email confirm'))
                }
            })
            .catch((err)=>{
                res.status(422).json(error(err))
            })
    }
}