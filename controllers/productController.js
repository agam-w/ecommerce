const {success, error} = require('../middlewares/response.js')
const multer  = require('multer')

const Product = require('../models/product')
const User = require('../models/user')

const storage = multer.diskStorage({
   
     destination: /* istanbul ignore next */function (req, file, cb) {
        cb(null, 'public/upload')
    },
     filename: /* istanbul ignore next */function (req, file, cb) {
        var filetype = ''
        /* istanbul ignore next */
        if (file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        else if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        else if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        /* istanbul ignore next */cb(null, file.fieldname + '-' + Date.now()+'.'+filetype)
    }
})
 
var upload = multer().single('img')

var ImageKit = require('imagekit');

var imagekit = new ImageKit({
  "imagekitId" : process.env.IMAGEKIT_ID,       
  "apiKey"     : process.env.IMAGEKIT_KEY,       
  "apiSecret"  : process.env.IMAGEKIT_SECRET, 
});

module.exports= {

    // upload.single('image'), //bisa ditulis seperti ini juga
    uploadImage(req,res){
        upload(req, res, function(err){
            var file = req.file
            if (!file) {
                return res.status(422).json(error("please upload file"))
            }

        var uploadPromise;
        uploadPromise = imagekit.upload(req.file.buffer.toString('base64'), {
            "filename" : req.file.originalname,
            "folder"   : "/product"
        });
       
        //handle upload success and failure
        uploadPromise.then((result)=>{
            Product.findByIdAndUpdate(req.params.id,{image:result.url}, (err, updt)=>{
                //if(err) return res.status(422).json(error("can't update url image"))   
                //res.status(200).json(success(updt, "url image has been update"))
                Product.findById(updt._id, (err, showUser)=>{
                    //if(err) return res.status(422).json(error("can't update url image"))   
                    res.status(200).json(success(showUser, "url image has been update"))
                })
            })     
        })
        .catch (err=>{
            res.status(400).json(error("can't upload file"))
        })
        })
    },

    create(req, res){
        Product.create(req.body, function (err, data){

            if (err) return res.status(400).json(error(err.message))
            User.findById(req.user._id, (err, user)=>{

                //if (err) return res.status(400).json(error("can't find user"))
                user.products.push(data)
                user.save()
                data.user=req.user._id
                data.save()
                res.status(201).json(success(data, "new product created"))
            })
            

        })
    },

    showAll(req, res){
        Product.find({}, (err, data)=>{
            res.status(200).json(success(data, "all product showed"))
        })
    },

    selectProduct(req, res){
        Product.findById(req.params.id, (err, data)=>{
            if(err) return res.status(403).json(error("product id false"))
            res.status(200).json(success(data, "selected product showed"))
        })
    },

    showMerchant(req, res){
        Product.find({user: req.user._d}, (err, data)=>{
            res.status(200).json(success(data, "all product merchant showed"))
        })
    },

    async update(req, res){
        try {
            let update = await Product.findByIdAndUpdate(req.params.id, req.body)
            res.status(200).json(success(update,"Product has been update"))
        }
        catch(err){
            res.status(400).json(error(err.message))
        }
    },

    delete(req, res){
        Product.findById(req.params.id).exec ((err, data)=>{

            if (err) return res.status(404).json(error("there is nothing to delete"))
            if (data.user._id != req.user._id) return res.status(404).json(error("this is not your post!"))
            
            Product.findByIdAndDelete(data._id, (err, product)=>{
                /* istanbul ignore if */if (product.img){
                    var ret = `${data.image}`.replace('https://ik.imagekit.io/agam/','')
                    var deletePromise = imagekit.deleteFile(`${ret}`);; 
                    deletePromise.then(function(resp) {})
                }

                //if (err) return res.status(422).json(error("there is nothing to delete"))
                User.update({ _id: req.user._id }, { $pullAll: { products: [req.params.id] } }, 
                    { safe: true, multi:true }, function(err, obj) {

                        //if (err) return res.status(422).json(error("can't delete from array"))
                        res.status(200).json(success(product ,"product deleted"))
                });
                
            })
        }) 
    }
}