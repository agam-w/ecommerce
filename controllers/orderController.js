const {success, error} = require('../middlewares/response.js')

const userOrder = require('../models/userOrder')
const Order = require('../models/order')
const Product = require('../models/product')

module.exports = {

    create(req, res){
        Order.create({product: req.params.id, quantity: req.body.quantity, user: req.user._id}, (err, data)=>{
            if(err) return res.status(400).json(error("product and user id false, or wrong value of request"))
            Order.findById(data._id).populate('product').exec((err, result)=>{
                result.subTotal = (result.product.price*result.quantity)
                result.save()
                userOrder.findOne({isDone: false, user: req.user._id},(err, userOrder)=>{
                    userOrder.orders.push(result)
                    userOrder.totalPrice += result.subTotal
                    userOrder.save()
                    res.status(201).json(success(userOrder, "order was added to cart"))
                })
            })
        })              
    },

    update(req, res){
        Order.findOneAndUpdate({_id: req.params.id, user: req.user._id}, {quantity: req.body.quantity})
            .populate('product').exec((err, data)=>{
                if (err) return res.status(422).json(error("product and user id false, or wrong value of request"))
                data.subTotal = (data.product.price*req.body.quantity)
                data.save()
                var deviation = ((req.body.quantity-data.quantity)*data.product.price)
                userOrder.findOne({isDone: false,user: req.user._id}, (err, result)=>{
                    result.totalPrice += deviation
                    result.save()
                    res.status(200).json(success(result, "update order done"))
                })
            }) 
    },

    delete(req, res){
        Order.findOne({_id: req.params.id, user: req.user._id},(err, data)=>{
            if (err) return res.status(422).json(error("product or user id false"))
            userOrder.findOneAndUpdate({isDone: false,user: req.user._id}, { $pullAll: { orders: [req.params.id] } }, 
                { safe: true, multi:true }, (err, obj)=>{
                    obj.totalPrice -= data.subTotal
                    obj.save()
                    Order.findByIdAndDelete(req.params.id).exec()
                    res.status(200).json(success(obj ,"order deleted"))
            })
        })
    },

    showCart(req, res){
        userOrder.findOne({isDone: false,user: req.user._id}).populate({ path: 'orders', populate: { path: 'product' }})
            .exec((err, data)=>{
                res.status(200).json(success(data, "active cart showed"))
        })
    },

    showCartHistory(req, res){
        userOrder.find({isDone: true,user: req.user._id}).populate({ path: 'orders', populate: { path: 'product' }})
            .exec((err, data)=>{
                res.status(200).json(success(data, "cart history showed"))
        })
    },

    async orderDone(req, res){
        
            var today = new Date();
            let purchase_no = 'INV'+today.getFullYear().toString().substr(-2)+(today.getMonth()+1)+today.getDate()+today.getSeconds();

            let cart = await userOrder.findOne({isDone: false,user: req.user._id})
                .populate({ path: 'orders', populate: { path: 'product', select: ['name','stock'] }})
            let length = cart.orders.length
            
            if (length>0){
                var prod = ''
                for (var i = 0; i < length; i++) {
                    let product_id = cart.orders[i].product._id
                    let order_quantity = cart.orders[i].quantity
                    
                    prod = await Product.findById(product_id)
                    let final_stock = prod.stock-order_quantity
                    if(order_quantity>prod.stock){
                        return res.status(422).json(error("Insufficient stock of "+prod.name))
                    }

                    updt_stock = {
                        stock: final_stock
                    }

                    await Product.findOneAndUpdate({"_id":product_id}, updt_stock)
                    // update stock in product done
        
                }

                cart.isDone = true
                cart.orderNo = purchase_no
                cart.save()
                await userOrder.create({user: req.user._id});
                return res.status(201).json(success(cart, "Order done"))

            }
            else{
                return res.status(422).json(error("Chart is empty"))
            }
        
    }
}