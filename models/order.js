const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderSchema = new Schema({
    
    user      : {type   : Schema.Types.ObjectId, 
                ref     : 'User'},
    product   : {type   : Schema.Types.ObjectId, 
                ref     : 'Product'},
    quantity  : {type   : 'number',
                required: true},
    subTotal  : {type   : 'number',
                default : 0}

  });

  module.exports = mongoose.model('Order', orderSchema);