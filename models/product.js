const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({

    name    : {type   :'string',
              required: true},
    price   : {type   : 'number',
              required: true},
    stock   : {type   : 'number',
              default : 0},
    category: {type   : 'string',
              required: true},
    detail  : {type   : 'string',
              required: false},
    user    : {type   : Schema.Types.ObjectId, 
              ref     : 'User'},
    image   : {required: false}

  });

  module.exports = mongoose.model('Product', productSchema);