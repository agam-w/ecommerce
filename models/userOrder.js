const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userOrderSchema = new Schema({
    
    user        : {type   : Schema.Types.ObjectId, 
                  ref     : 'User'},
    orders      : [{type  : Schema.Types.ObjectId, 
                  ref     : 'Order'}],
    totalPrice  : {type   : 'number',
                  default : 0},
    isDone      : {type   : 'boolean',
                  default : false},
    date        : {type   : 'date',
                  default : Date.now()},
    orderNo     : {type   : 'string'}

  });

  module.exports = mongoose.model('userOrder', userOrderSchema);